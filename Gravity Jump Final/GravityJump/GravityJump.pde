import processing.sound.*;

Camera c;
Player p;
PipeSet[] ps;
static final int NUMPIPES = 1000;
int numPipes;
static final int GAMEOVER = 1;
static final int GAME = 0;
int gameState;
PImage pipe;
PImage space;
PImage astronaut;
PImage astronaut2;
PImage floor;
PImage over;
PImage menu;
PImage credits;
PImage ins;
PImage cajita;
int powerupid;
int vida;
int contPUs = 0;
boolean gravedad = false, freno = false;
ArrayList<PowerUp> pus;
PFont font;
int highscore = 0;
SoundFile bgm;
SoundFile jump;
SoundFile lose;
SoundFile crash;
 
void setup() {
  //Tamaño del juego
  size(960, 512);
  initGame();
  pipe = loadImage("pipe.png");
  space = loadImage("back.png");
  astronaut = loadImage("astronaut.png");
  astronaut2 = loadImage("astronaut2.png");
  floor = loadImage("floor.jpg");
  over = loadImage("gameover.png");
  credits = loadImage("credits.png");
  menu = loadImage("Menu.png");
  ins = loadImage("instructions.png");
  cajita = loadImage("powerUp.gif");
  font = createFont("8bit.TTF", 20);
  textFont(font);
   // Load a soundfile from the /data folder of the sketch and play it back
  bgm = new SoundFile(this, "Arpanauts.mp3");
  jump = new SoundFile(this, "jump.mp3");
  jump.amp(0.02);
  lose = new SoundFile(this, "lose.mp3");
  lose.amp(0.1);
  crash = new SoundFile(this, "crash.wav");
  crash.amp(0.5);
  bgm.loop();

}
 
void draw() {
  switch(gameState) {
    case 1:
      background(over);
      fill(255);
      text("GAME OVER", width/2-textWidth("GAME OVER")/2 - 230, height/2 - 150);
      text("Press enter to continue", width/2-textWidth("Press enter to continue")/2 - 230, height/2 - 80);
      text("Score " + p.score, width/2-textWidth("Score: 00")/2 - 230, height/2 - 20);
      text("highscore " + highscore, width/2-textWidth("highscore: 000")/2 - 230, height/2 + 40);
      
      if (keyPressed) {
        if(key == ENTER){
          lose.stop();
          bgm.loop();
          initGame();
          gameState = 0;
      }
      }
      
      break;
    case 0:
    background(menu);
    text("highscore " + highscore,720,500);
    if (keyPressed) {
      if(key == 'S' || key  == 's'){
        initGame();
        gameState = 2;
      }
      if(key == 'I' || key  == 'i'){
        gameState = 3;
      }
      if(key == 'C' || key  == 'c'){
        gameState = 4;
      }
    }
    break;
    case 2:
    drawBG();
      fill(0, 255, 0);
      translate(-c.pos.x, -c.pos.y);
      p.draw(c);
      c.draw();
      drawPowerUps();
      drawPipes();
      translate(c.pos.x, c.pos.y);
      drawFG();
      drawHUD();
      //Colisión piso
      if (p.pos.y  > height - 120 || p.pos.y < -20)  gameState = GAMEOVER;
      break;
    
    case 3:
    background(ins);
     if (keyPressed) {
      if(key == BACKSPACE){
        gameState = 0;
      }
    }
    break;
    
    case 4:
    background(credits);
     if (keyPressed) {
      if(key == BACKSPACE){
        gameState = 0;
      }
    }
    break;
  }
}
 
 
void initGame() {
  gameState = 0;
  ps = new PipeSet[NUMPIPES];
  pus = new ArrayList();
  c = new Camera();
  p = new Player();
  makePipes();
  vida = 0;
  gravedad = false;
  freno = false;
}
void reinitGame(int pscore) {
  gameState = 2;
  ps = new PipeSet[NUMPIPES];
  pus = new ArrayList();
  c = new Camera();
  p = new Player();
  makePipes();
  p.score = pscore;
  gravedad = false;
  freno = false;
}
 
void drawBG() {
  //Fondo de pantalla
  background(space);
  //ellipse(width/2, height-150, width+150, 100);
  //Fondo planeta
  //fill(0, 128, 255);
  //noStroke();
  //Dibujo del planeta
  //ellipse(width/2, height-125, width+125, 125);
}
 
void drawFG() {
  //Piso
  //Color Piso
 
  fill(150, 142, 150);
  noStroke();
  rect(-20, height-100, width+50, 300);
  stroke(0);
  //Color Línea
  fill(153, 153, 150);
  rect(-20, height-110, width+50, 10);
}
 
void makePipes() {
  //Distancia inicial a la primera pipa
  float x = 650;
  float y = random(200, 300);
  //Espacio entre pipas en Y
  float h = random(100, 125);
 
  //Distancia y generación de las siguientes pipas
  for (int i = 0; i < NUMPIPES; i++) {
    ps[i] = new PipeSet(x, y, h);
    y = random(150, 300);
    //Espacio entre pipas en Y
    h = random(100, 125);
    //Espacio entre pipas en X
    x += random(300, 400);
    //agregar PUs después de la pipa
      pus.add(new PowerUp((int)x, i));
      contPUs++;
  }
  println("cont: " + contPUs);
  println("size: " + pus.size());
}
 
void drawPipes() {
  //Colisión y Puntuación
  for (int i = 0; i < NUMPIPES; i++) {
    if (PVector.dist(p.pos, ps[i].pos) < 650)
      ps[i].draw();
    if (p.pos.x > ps[i].pos.x && p.pos.x < ps[i].pos.x + ps[i].bWidth && p.pos.y+100 > ps[i].pos.y && p.pos.y+100 < ps[i].pos.y + ps[i].bHeight)
    {
      if (!ps[i].scored)
        p.score++;
        highscore = max(p.score, highscore);
      ps[i].scored = true;
    }
    if (p.pos.x > ps[i].pos.x && p.pos.x < ps[i].pos.x + ps[i].bWidth && (p.pos.y+100 < ps[i].pos.y || p.pos.y+100 > ps[i].pos.y + ps[i].bHeight)) {
      vida--;
      if(vida < 0){
        crash.play();
        bgm.stop();
        lose.loop();
        gameState = GAMEOVER;
      } else{
        reinitGame(p.score);
      }
    }
  }
}
void drawPowerUps(){
  for(int i = 0; i < pus.size(); i++){
    if(PVector.dist(p.pos, pus.get(i).posPU) < 650){
      pus.get(i).drawPU();
    }
    if(pus.get(i).chocaPU((int)p.pos.x, (int)p.pos.y)){
      println(powerupid);
      pus.remove(i);
    }
  }
}
 
void drawHUD() {
  textSize(20);
  fill(0);
  text("Distance " + (int)(p.pos.x/100), 0, height - 40);
  text("Score " + p.score, 0, height - 10);
  if(vida > 0){
    text("Extra Lives " + vida, width - 300, height - 10);
  }
  if(gravedad){
    text("Gravity switch", width - 280, height - 40);
  }
  if(freno){
    text("Slown down", width - 230, height - 40);
  }
}

 
class Camera {
  PVector pos; 
 
  Camera() {
    pos = new PVector(0, 0);
  }
 
  void draw() {
    if(!freno){
      pos.x += 7;
    } else pos.x += 4;
  }
}

class Player {
  PVector pos;
  int score;
 
  Player() {
    score = 0;
    pos = new PVector(100, 250);
  }
 
  void draw(Camera c) {
    drawPlayer();
 
    if (keyPressed) { 
      if (key == ' ') {
        if(!gravedad){
          image(astronaut2, pos.x, pos.y);
          pos.y -= 9;
        }
        else{
          //se puede cambiar la imagen?!?!
          image(astronaut2, pos.x, pos.y);
          pos.y +=9;
        }
      }
    }
    if(gravedad){
      pos.y -= 1.7;
      pos.x += 7;
    }
    else if(freno){
      pos.y += 1.7;
      pos.x += 4;
    }
    else{
      pos.y += 1.7;
      pos.x += 7;
    }
  }
 
  void drawPlayer() {
    //Dibujar jugador
    image(astronaut, pos.x, pos.y);
  }
}

class PipeSet {
  PVector pos;
  float bHeight, bWidth;
  boolean scored;
 
  PipeSet(float x, float y, float h) {
    pos = new PVector(x, y);
    bHeight = h;
    bWidth = 50;
    scored = false;
  }
 
  void draw() {
    drawPipes();
  }
 
  void drawPipes() {
    //Color pipes
    fill(160, 160, 160);
    image(pipe, pos.x, -100, 50, pos.y);
    image(pipe, pos.x, pos.y+bHeight-100, 50, height);
  }
}

class PowerUp {
  int pux, puy;
  PVector posPU;
  PowerUp(){
    pux = -1;
    puy = -1;
    posPU = new PVector(pux, puy);
  }
/*  PowerUp(int psx){
    powerupid = definePU();
    positionPU(psx);
    posPU = new PVector(pux, puy);
  }*/
  PowerUp(int psx, int level){
    powerupid = definePU(level);
    positionPU(psx);
    posPU = new PVector(pux, puy);
  }
/*  int definePU(){
    powerupid = 0;
    if(p.score > 15){
      powerupid = (int)random(0, 1);
    } //Vidas
    if(p.score > 25){
      powerupid = (int)random(0, 2);
    } //Cambio de Gravedad
    if(p.score > 35){
      powerupid = (int)random(0, 3);
    } //Frenado
    return powerupid;
  }*/
  int definePU(int l){
    powerupid = 0;
    if(l > 15){
      powerupid = (int)random(0, 1);
    } //Vidas
    if(l > 25){
      powerupid = (int)random(0, 2);
    } //Cambio de Gravedad
    if(l > 35){
      powerupid = (int)random(0, 3);
    } //Frenado
    return powerupid;
  }
  void positionPU(int psx){
    pux = psx + (int)random(105, 120); //espacio después de la pipa en X
    puy = (int)random(12, 500); //altura 
  }
  void drawPU(){
    if(powerupid == 1 || powerupid == 2 || powerupid == 3){
      image(cajita, pux, puy);
    }
  }
  boolean chocaPU(int plx, int ply){
    if (plx > pux && plx < pux + 40 && ply > puy && ply < puy + 40){
      switch(powerupid){
        case 1:
        vida++;
        gravedad = false;
        freno = false;
        break;
        case 2:
        gravedad = !gravedad;
        freno = false;
        break;
        case 3:
        freno = !freno;
        gravedad = false;
        break;
      }
      return true;
    }
    else return false;
  }
}